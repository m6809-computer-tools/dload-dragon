
        org     $7C00

* install our command dispatch hook
setup   lda     #$7E		; JMP opcode
        ldx     #dispatch_hook
        stx     $0179+1
        sta     $0179
        ldx     #console_in_hook
        stx     $016a+1
        sta     $016a
        rts

TOKEN_DLOAD equ   $B9

* DEBUG equ 1

dispatch_hook
        cmpa    #TOKEN_DLOAD
        beq     dload
        rts
dload   leas    2,s		; throw away return address, replacing caller
        jsr     <$9f		; read next char
        jsr     <$a5		; peek at next char
        cmpa    #'X		; DLOADXM ?
        bne     go		; if not, do not return here
        jsr     <$9f		; read next char (the X)
        jsr     go		; return here
        jmp     [$9d]		; EXEC address
go      lbra    DLOAD_dispatch	; run the recompiled dispatch routine

console_in_hook
        lda     <$6f		; DEVNUM
        cmpa    #$FD		; -3 = DLOAD
        beq     indload
        rts
indload leas    2,s		; replace caller
        IFDEF   DEBUG
        lda     #'.
        jsr     $bcab 		; %OUTCH% to screen
        ENDC
* "inspired" by what normal console_in does
* just stripped of DEVNUM checks
* what matters is that it jumps into our new L_A106
        clr     <$70      ; reset EOF flag
        tst     <$79      ; cassette buffer size
        bne     L_B51A    ; branch if buffer space
        com     <$70      ; set EOF
        rts               ; and return
L_B51A  pshs    b,x,y,u
        ldx     <$7a      ; header buffer address
        lda     ,x+
        pshs    a
        stx     <$7a
        dec     <$79      ; decrement buffer count
        bne     L_B531    ; return if buffer not empty
        jsr     L_A106    ; get next block into buffer
        IFDEF   DEBUG
        lda     #'+
        jsr     $bcab 		; %OUTCH% to screen
        ENDC
L_B531  puls    a,b,x,y,u,pc	; return


* These are NOPs in original Dragon 32 ROM, hook in the 
* DriveWire low-level serial in/out routines instead

call_SEROUT
        pshs    x,y
        ldy     #1		; send one byte
        ldx     #buffer
        sta     ,x
        IFDEF DEBUG
        lda     #'!
        jsr     $bcab 		; %OUTCH% to screen
        lda     ,x
        jsr     dumpbyt		; debug
        lda     #32
        jsr     $bcab 		; %OUTCH% to screen
        lda     ,x
        ENDC
        lbsr    DWWrite
        puls    x,y,pc

call_SERIN
        pshs    x,y,b
        ldy     #1
        ldx     #buffer
        lbsr    DWRead
        pshs    cc
        IFDEF   DEBUG
        beq     readok
        pshs    a,cc
        lda     #'B+32
        jsr     $bcab 		; %OUTCH% to screen
        puls    a,cc
readok
        bcc     frameok
        lda     #'F+32
        jsr     $bcab 		; %OUTCH% to screen
frameok
        lda     ,x
        tsta
        bne     logit
        lda     #'0
        jsr     $bcab
        bra     debugend
logit   lda     #'<
        jsr     $bcab
        jsr     dumpbyt
        lda     #$20
        jsr     $bcab 	; space
debugend
        ENDC
        lda     ,x
        puls    x,y,b,cc,pc

buffer  fcb     0

        IFDEF DEBUG
dumpbyt lda     ,x
        lsra
        lsra
        lsra
        lsra
        CMPA    #9         ; Check for decimal digit
        BLS     ADDCHO
        ADDA    #7         ; A to F offset
ADDCHO  ADDA    #'0        ; Convert to character
        jsr     $bcab      ; %OUTCH% to screen
        lda     ,x
        ANDA    #$F        ; Mask off MS 4 bits
        CMPA    #9         ; Check for decimal digit
        BLS     ADDCHU
        ADDA    #7         ; A to F offset
ADDCHU  ADDA    #'0        ; Convert to character
        jsr     $bcab      ; %OUTCH% to screen
        rts
        ENDC

* This code is from the BASIC ROM

        include dload-dispatch.asm

* referenced BASIC ROM routines in disassembled code

Get8Bit  equ   $8E51
Get16Bit equ   $8E83
CkComa   equ   $89AA
erase_basic equ   $8417
console_in equ   $B50A
FM_error equ   $B848
IO_error equ   $B84B
FC_error equ   $8B8D
system_error equ   $8344
L_837A   equ   $837A		; go to main loop, read program from DEVNUM
L_B876   equ   $B876		; update ...
L_B65F   equ   $B65F		; close all files
L_B7F9   equ   $B7F9		; SN ERROR if not EOL
L_B7AA   equ   $B7AA
L_B87A   equ   $B87A
L_B73C   equ   $B73C
L_B867   equ   $B867		; read from DEVNUM
L_B663   equ   $B663		; close file

* CoCo 1/2 Initialization Code (copied from dw3dos.asm)
call_SERSET
         ldx   #PIA1Base               PIA1
         clr   -3,x                    clear PIA0 Control Register A
         clr   -1,x                    clear PIA0 Control Register B
         clr   -4,x                    set PIA0 side A to input
         ldd   #$FF34
         sta   -2,x                    set PIA0 side B to output
         stb   -3,x                    enable PIA0 peripheral reg, disable PIA0
         stb   -1,x                    MPU interrupts, set CA2, CA1 to outputs
         clr   1,x                     $FF20 = DDR, motoroff
         clr   3,x                     $FF22 = DDR, sound disabled
         deca                          A = $FE after deca
         sta   ,x                      bits 1-7 are outputs, bit 0 is input on PIA1 side A
         lda   #$F8
         sta   2,x                     bits 0-2 are inputs, bits 3-7 are outputs on B side
         stb   1,x                     enable peripheral registers, disable PIA1 MPU
         stb   3,x                     interrupts and set CA2, CB2 as outputs
         clr   2,x                     set 6847 mode to alphanumeric
         ldb   #$02
         stb   ,x                      make RS-232 output marking
         clrb
         tfr   b,dp                    B = 0
         ldb   #$04
         clr   -2,x
         bitb  2,x

         lda   #$37
         sta   PIA1Base+3

         lda   PIA0Base+3
         ora   #$01
         sta   PIA0Base+3

         lda   PIA1Base+2
         anda  #$07
         sta   PIA1Base+2

* Spin for a while so that the RS-232 bit stays hi for a time
Reset
         ldx   #$A000
Spin     leax  -1,x
         bne    Spin
         rts

         IFEQ  DW4-1
         use   hdbdos/dw4read.asm 
         use   hdbdos/dw4write.asm 
         ELSE
         use   hdbdos/dwread.asm 
         use   hdbdos/dwwrite.asm 
         ENDC

* defines for DriveWire functions

IntMasks equ   $50
Carry    equ   1
PIA0Base equ   $FF00
PIA1Base equ   $FF20
DAT.Regs equ   $FFA0
E$NotRdy equ   246
Vi.PkSz  equ   0
V.SCF    equ   0
         use   hdbdos/dwdefs.d

         end setup
