
AS = lwasm -r --pragma=condundefzero
MAKEWAV = makewav -v -r

dload-dw.wav: dload-dw.bin
	$(MAKEWAV) -c -o$@ -nDLOADDW3 $<

dload-dw.bin: dload-dw.asm dload-dispatch.asm
	$(AS) -o$@ $< -DCoCo=1 -DBIN=1 -b -DBASIC -l > /tmp/dload.lst

dload-dispatch.asm: rom.asm
	sed -n '/^DLOAD_dispatch/,/^L_A1C9/p' < $< > $@
	sed -i \
	 -e s/\$$802a/call_SERIN/ \
	 -e s/\$$802d/call_SEROUT/ \
	 -e s/\$$8030/call_SERSET/ \
	 -e s/\$$a0f4/L_A0F4/ \
	 -e s/\$$a17e/L_A17E/ \
	 -e s/\$$a1c1/L_A1C1/ \
	 $@
	 

rom.asm: Dragon\ Data\ Ltd\ -\ Dragon\ 32\ -\ IC17.ROM
	./6809dasm.pl org=0x8000 \
	 fcb=0x8033,0x8173 \
	 fcb=0x8174,0x82f6 \
	 fcb=0x9fce,0x9fff \
	 fdb=0xa000,0xa00d \
	 label=DLOAD_dispatch,0xa049 \
	 label=call_SERIN,0x802a \
	 label=call_SEROUT,0x802d \
	 label=call_SERSET,0x8030 \
	 label=FC_error,0x8b8d \
	 label=L_A106,0xa106 \
	 label=L_A0F4,0xa0f4 \
	 label=L_A17E,0xa17e \
	 label=L_A1C1,0xa1c1 \
	 "$<" > $@

clean:
	rm -f rom.asm dload-dispatch.asm dload-dw.bin dload-dw.wav
